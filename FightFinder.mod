<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="Fight Finder" version="1.0" date="06/22/2009" >
        
        <Author name="Werit" email="weritblog@gmail.com" />
        <Description text="Notifies you of fights in every tier." />
        
        <Dependencies>
	    <Dependency name="EA_ChatWindow"/>
        </Dependencies>
        
        <Files>
            <File name="FightFinder.lua" />
        </Files>
        
        <OnInitialize>
            <CallFunction name="FightFinder.init" />
        </OnInitialize>
        <OnUpdate/>
        <OnShutdown/>
    </UiMod>
</ModuleFile>
