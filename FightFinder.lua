FightFinder = {}
FightFinder.zoneHotSpotData = {}

--Thanks wareffort!
FightFinder.keepNames = {
  [1]=L"Dok Karaz",
  [2]=L"Fangbreaka Swamp",
  [3]=L"Gnol Baraz",
  [4]=L"Thickmuck Pit",
  [5]=L"Karaz Drengi",
  [6]=L"Kazad Dammaz",
  [7]=L"Bloodfist Rock",
  [8]=L"Karak Karag",
  [9]=L"Ironskin Skar",
  [10]=L"Badmoon Hole",
  [11]=L"Mandred's Hold",
  [12]=L"Stonetroll Keep",
  [13]=L"Passwatch Castle",
  [14]=L"Stoneclaw Castle",
  [15]=L"Wilhelm's Fist",
  [16]=L"Morr's Repose",
  [17]=L"Southern Garrison",
  [18]=L"Garrison of Skulls",
  [19]=L"Zimmeron's Hold",
  [20]=L"Charon's Keep",
  [21]=L"Cascades of Thunder",
  [22]=L"Spite's Reach",
  [23]=L"Well of Qhaysh",
  [24]=L"Ghrond's Sacristy",
  [25]=L"Arbor of Light",
  [26]=L"Pillars of Remembrence",
  [27]=L"Covenant of Flame",
  [28]=L"Drakebreaker's Scourge",
  [29]=L"Hatred's Way",
  [30]=L"Wrath's Resolve"
}

FightFinder.attackStatus = {
  [1]=L"Keep is Safe.",
  [2]=L"Outer Walls Under Attack!",
  [3]=L"Inner Walls Under Attack!",
  [4]=L"Keep Lord Under Attack!",
  [5]=L"Keep Taken!"
}

FightFinder.kTable = {}

FightFinder.keepOn = 1

function FightFinder.keepFind()

    local cnt = 0 

    if (FightFinder.keepOn == 0) then
        return
    end

     for i=1,30 do

         local x = GetKeepData(i)
         if (x == nil) then
              continue
	 end

	 if (FightFinder.kTable[i] == nil) then
             FightFinder.kTable[i] = x.attackStatus
             if( x.attackStatus ~= 1 ) then
	         EA_ChatWindow.Print(towstring(FightFinder.keepNames[x.id])..towstring(": ")..towstring(FightFinder.attackStatus[x.attackStatus]),SystemData.ChatLogFilters.RVR)
	     end
	 else
             if (FightFinder.kTable[i] ~= x.attackStatus) then
                 if( x.attackStatus ~= 1 ) then
		     EA_ChatWindow.Print(towstring(FightFinder.keepNames[x.id])..towstring(": ")..towstring(FightFinder.attackStatus[x.attackStatus]),SystemData.ChatLogFilters.RVR)
                     FightFinder.kTable[i] = x.attackStatus
		 else
                     FightFinder.kTable[i] = x.attackStatus
		 end
	     end
         end
    end
	
end


function FightFinder.init()
    EA_ChatWindow.Print(towstring("Starting Fight Finder!"))
    
    LibSlash.RegisterSlashCmd("ff", FightFinder.handleSlash)

    FightFinder.update()
    FightFinder.keepFind()

    RegisterEventHandler(SystemData.Events.PAIRING_MAP_HOTSPOT_DATA_UPDATED,"FightFinder.update")
    RegisterEventHandler(SystemData.Events.PAIRING_MAP_HOTSPOT_DATA_UPDATED,"FightFinder.keepFind")
end

function FightFinder.handleSlash(input)

    local args=FightFinder.explode(" ",input);
    if args[1] == "help" then
        FightFinder.help()
    elseif args[1] == "keeps" then
        if args[2] == "on" then
	   FightFinder.keepOn = 1
           EA_ChatWindow.Print(towstring("Fight Finder now tracking keeps."))
	else
	   FightFinder.keepOn = 0
           EA_ChatWindow.Print(towstring("Fight Finder keep tracking disabled."))
	end
    else
        FightFinder.help()
    end
end

function FightFinder.help()
  
    EA_ChatWindow.Print(towstring("Fight Finder Help v1.1"))
    EA_ChatWindow.Print(towstring("/ff keeps on - Enable Keep tracking"))
    EA_ChatWindow.Print(towstring("/ff keeps off - Disable Keep tracking"))

end

--Thanks mireaclegrow!
function FightFinder.explode(div,str)
    if (div=='') then return false end
    local pos,arr = 0,{}
    for st,sp in function() return string.find(str,div,pos,true) end do
        table.insert(arr,string.sub(str,pos,st-1))
        pos = sp + 1
    end
    table.insert(arr,string.sub(str,pos))
    return arr
end

function FightFinder.update()

    for i=1,3 do
        for zoneId, iconType in pairs( EA_Window_WorldMap.pairingMapZones[i] )
            do
		hotspotSize = GameData.HotSpotSize.NONE
                hotspotSize = GetZoneLargestHotspotSize( zoneId )
		--local zonename = GetZoneName(zoneId)
                --EA_ChatWindow.Print(towstring("[!] "..hotspotSize .." fighting reported in ")..zonename..towstring("!"),SystemData.ChatLogFilters.RVR)
	        if (hotspotSize ~= GameData.HotSpotSize.NONE) then
		    
		    if (FightFinder.zoneHotSpotData[zoneId] ~= nil) then
                        oldHotspotSize = FightFinder.zoneHotSpotData[zoneId]
	            end 

		    if (hotspotSize ~= oldHotspotSize) then
		        zonename = GetZoneName(zoneId)
		        size = "" 
		        if ( hotspotSize == GameData.HotSpotSize.SMALL) then
			        size = "Light"
  	                elseif  ( hotspotSize == GameData.HotSpotSize.MEDIUM) then
			        size = "Medium"
	                elseif  ( hotspotSize == GameData.HotSpotSize.LARGE) then
			        size = "Heavy"
                        end
                  
                        EA_ChatWindow.Print(towstring("[!] "..size .." fighting reported in ")..zonename..towstring("!"),SystemData.ChatLogFilters.RVR)
                        FightFinder.zoneHotSpotData[zoneId] = hotspotSize

	            end
		else
		    if (FightFinder.zoneHotSpotData[zoneId] ~= nil) then
                        FightFinder.zoneHotSpotData[zoneId] = nil
	            end 
                end
        end
    end
end
